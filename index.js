//without the use of objects, our students from before would be organized as follows if we are to record additional information about them

/*
    Spaghetti Code - When code is not organized enough that it becomes hard to work it.

    Encapsulation Code - Orginized related information(properties) and behavior(method) to belong to a single entity.
*/

//create student one
// let studentOneName = 'John';
// let studentOneEmail = 'john@mail.com';
// let studentOneGrades = [89, 84, 78, 88];

// //create student two
// let studentTwoName = 'Joe';
// let studentTwoEmail = 'joe@mail.com';
// let studentTwoGrades = [78, 82, 79, 85];

// //create student three
// let studentThreeName = 'Jane';
// let studentThreeEmail = 'jane@mail.com';
// let studentThreeGrades = [87, 89, 91, 93];

// //create student four
// let studentFourName = 'Jessie';
// let studentFourEmail = 'jessie@mail.com';
// let studentFourGrades = [91, 89, 92, 93];

//actions that students may perform will be lumped together
// function login(email){
//     console.log(`${email} has logged in`);
// }

// function logout(email){
//     console.log(`${email} has logged out`);
// }

// function listGrades(grades){
//     grades.forEach(grade => {
//         console.log(grade);
//     })
// }

//This way of organizing employees is not well organized at all.
//This will become unmanageable when we add more employees or functions
//To remedy this, we will create objects

  //////////////
 //   QUIZ   //
//////////////
/*
    1. Spaghetti Code
    2. Using []
    3. Encapsulation
    4. student1.enroll()
    5. TRUE
    6. it represent the function where it was called.
    7. TRUE
    8. TRUE
    9. TRUE
    10. TRUE
*/
  ///////////////////////
 //  FUNCTION CODING  //
///////////////////////
let student1 = {
    name:'John',
    email:'john@mail.com',
    grades:[89, 84, 78, 88],
    login:function () {
            //.this - when inside a method, refers to the object where it is in. 
            console.log(`${this.email} has logged in`);
        },
    logout:function (){
            console.log(`${this.email} has logged out`);
        },
    listGrades:function (){
            this.grades.forEach(grade => {
                console.log(grade);
            })
        },
    computeAve: function(){
        let leng = this.grades.length;
        let ave = 0;
        this.grades.forEach(grade =>{
            ave += grade;
        })
        ave = ave / leng;
        return ave;
    },
    willPass: function() {
        if(this.computeAve() >= 85) {
            return true;
        } else{
            return false;
        }
    },
    willPassWithHonor: function() {
        if(this.computeAve() >= 90){
            return true;
        } else if(this.computeAve() >= 85 && this.computeAve() <90) {
            return false;
        } else{
            return undefined;
        }
    }
};

let student2 = {
    name:'Joe',
    email:'joe@mail.com',
    grades:[78, 82, 79, 85],
    login:function () {
            console.log(`${this.email} has logged in`);
        },
    logout:function (){
            console.log(`${this.email} has logged out`);
        },
    listGrades:function (){
            this.grades.forEach(grade => {
                console.log(grade);
            })
        },
    computeAve: function(){
        let leng = this.grades.length;
        let ave = 0;
        this.grades.forEach(grade =>{
            ave += grade;
        })
        ave = ave / leng;
        return ave;
    },
    willPass: function() {
        if(this.computeAve() >= 85) {
            return true;
        } else{
            return false;
        }
    },
    willPassWithHonor: function() {
        if(this.computeAve() >= 90){
            return true;
        } else if(this.computeAve() >= 85 && this.computeAve() <90) {
            return false;
        } else{
            return undefined;
        }
    }
};

let student3 = {
    name:'Jane',
    email:'jane@mail.com',
    grades:[87, 89, 91, 93],
    login:function () {
            console.log(`${this.email} has logged in`);
        },
    logout:function (){
            console.log(`${this.email} has logged out`);
        },
    listGrades:function (){
            this.grades.forEach(grade => {
                console.log(grade);
            })
        },
    computeAve: function(){
        let leng = this.grades.length;
        let ave = 0;
        this.grades.forEach(grade =>{
            ave += grade;
        })
        ave = ave / leng;
        return ave;
    },
    willPass: function() {
        if(this.computeAve() >= 85) {
            return true;
        } else{
            return false;
        }
    },
    willPassWithHonor: function() {
        if(this.computeAve() >= 90){
            return true;
        } else if(this.computeAve() >= 85 && this.computeAve() <90) {
            return false;
        } else{
            return undefined;
        }
    }
};

let student4 = {
    name:'Jessie',
    email:'jessie@mail.com',
    grades:[91, 89, 92, 93],
    login:function () {
            console.log(`${this.email} has logged in`);
        },
    logout:function (){
            console.log(`${this.email} has logged out`);
        },
    listGrades:function (){
            this.grades.forEach(grade => {
                console.log(grade);
            })
        },
    computeAve: function(){
        let leng = this.grades.length;
        let ave = 0;
        this.grades.forEach(grade =>{
            ave += grade;
        })
        ave = ave / leng;
        return ave;
    },
    willPass: function() {
        if(this.computeAve() >= 85) {
            return true;
        } else{
            return false;
        }
    },
    willPassWithHonor: function() {
        if(this.computeAve() >= 90){
            return true;
        } else if(this.computeAve() >= 85 && this.computeAve() <90) {
            return false;
        } else{
            return undefined;
        }
    }
};